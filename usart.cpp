/*
 * uart.c
 *
 *  Created on: 2012/09/28
 *      Author: 岩滝　宗一郎
 */

#include "stm32f4xx_conf.h"
#include "usart.h"

#define USART_SENDING 1
#define USART_NOT_SENDING 0
//C++用Usart class
Usart::Usart(int ch,uint32_t uart_brr){
	init_usart(ch,uart_brr);
	this->ch=ch;
	switch(ch){
	case 1:
		this->info.name="USART1";
		this->info.Readfunction=usart1_getc;
		this->info.Writefunction=usart1_putc;
		this->info.IsNotEmptyfunc=usart1_is_received;
		break;
	case 2:
		this->info.name="USART2";
		this->info.Readfunction=usart2_getc;
		this->info.Writefunction=usart2_putc;
		this->info.IsNotEmptyfunc=usart2_is_received;
		break;
	case 6:
		this->info.name="USART6";
		this->info.Readfunction=usart6_getc;
		this->info.Writefunction=usart6_putc;
		this->info.IsNotEmptyfunc=usart6_is_received;
		break;
	}
	this->info.fd=StreamIO_registration(&(this->info));
}
void Usart::putc(unsigned char c){
	switch(this->ch){
	case 1:
		usart1_putc(c);
		break;
	case 2:
		usart2_putc(c);
		break;
	case 6:
		usart6_putc(c);
		break;
	}
}
char Usart::getc(void){
	switch(this->ch){
	case 1:
		return usart1_getc();
	case 2:
		return usart2_getc();
	case 6:
		return usart6_getc();
	}
}
int Usart::write(char*buf,int count){
return -1;
}
int Usart::read(char*buf,int count){
return -1;
}

//以下，Cのプログラム
/*
 * USARTようにGPIOを初期化する
 *
 */
void gpio_init_for_uart(GPIO_TypeDef*GPIOx,uint16_t txpin,uint16_t rxpin);//initialize gpio for usart
void gpio_init_for_uart(GPIO_TypeDef*GPIOx,uint16_t txpin,uint16_t rxpin){
	GPIO_InitTypeDef gpioinitstr;
	gpioinitstr.GPIO_Mode=GPIO_Mode_AF;//TX Pin
	gpioinitstr.GPIO_Pin=txpin;
	gpioinitstr.GPIO_Speed=GPIO_Speed_50MHz;
	gpioinitstr.GPIO_OType=GPIO_OType_PP;
	GPIO_Init(GPIOx,&gpioinitstr);
	gpioinitstr.GPIO_Mode=GPIO_Mode_AF;//RX Pin
	gpioinitstr.GPIO_PuPd=GPIO_PuPd_UP;
	gpioinitstr.GPIO_Pin=rxpin;
	GPIO_Init(GPIOx,&gpioinitstr);
}
/*
 * USART本体を初期化
 */
void usartmodule_init(USART_TypeDef*USARTx,uint32_t brr);//initialize usart
void usartmodule_init(USART_TypeDef*USARTx,uint32_t brr){
	USART_InitTypeDef usartinitstr;
	usartinitstr.USART_BaudRate=brr;//通信パラメータ設定
	usartinitstr.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	usartinitstr.USART_Parity=USART_Parity_No;
	usartinitstr.USART_StopBits=USART_StopBits_1;
	usartinitstr.USART_WordLength=USART_WordLength_8b;
	usartinitstr.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;

	USART_Init(USARTx,&usartinitstr);
}
/*
 * DMAを初期化
 */
#ifdef _USE_DMA_
void dma_init_for_uart(DMA_Stream_TypeDef*DMA_Channel,USART_TypeDef*usartx,uint32_t buffaddr,int ch);//initialize dma
void dma_init_for_uart(DMA_Stream_TypeDef*DMA_Channel,USART_TypeDef*usartx,uint32_t buffaddr,int ch){
	DMA_InitTypeDef dmainitstr;
	dmainitstr.DMA_PeripheralBaseAddr=(uintptr_t)(&usartx->DR);
	dmainitstr.DMA_PeripheralDataSize=DMA_PeripheralDataSize_HalfWord;
	dmainitstr.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
	dmainitstr.DMA_DIR=DMA_DIR_PeripheralToMemory;
	dmainitstr.DMA_BufferSize=BUFF_SIZE;
	dmainitstr.DMA_Memory0BaseAddr=buffaddr;
	dmainitstr.DMA_MemoryDataSize=DMA_MemoryDataSize_HalfWord;
	dmainitstr.DMA_MemoryInc=DMA_MemoryInc_Enable;
	dmainitstr.DMA_Mode=DMA_Mode_Circular;
	dmainitstr.DMA_Channel=ch;
	dmainitstr.DMA_Priority=DMA_Priority_Medium;
	dmainitstr.DMA_PeripheralBurst=DMA_PeripheralBurst_Single;
	dmainitstr.DMA_FIFOMode=DMA_FIFOMode_Disable;
	dmainitstr.DMA_MemoryInc=DMA_MemoryBurst_Single;
	DMA_Init(DMA_Channel,&dmainitstr);
	DMA_FlowControllerConfig(DMA_Channel,DMA_FlowCtrl_Memory);
}
#endif
/*
 * 割り込みの設定
 */
void nvic_init_for_uart(uint8_t NVIC_IRQChannel);//nvic initialize
void nvic_init_for_uart(uint8_t NVIC_IRQChannel){
	NVIC_InitTypeDef nvicinitstr;
	nvicinitstr.NVIC_IRQChannel=NVIC_IRQChannel;
	nvicinitstr.NVIC_IRQChannelCmd=ENABLE;
	nvicinitstr.NVIC_IRQChannelPreemptionPriority=3;
	nvicinitstr.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&nvicinitstr);
}
/*
typedef struct ringbuff{
	int write_p,read_p,state;
	char data[BUFF_SIZE];
} ringbuff_t;//ソフトウエアリングバッファ
typedef struct ringbuff_dma{
	int read_ptr;
	char data[BUFF_SIZE];
} ringbuf_dma_t;//dmaによる受信バッファ
*/

ringbuff_t usart1_tx_buf,usart2_tx_buf,usart6_tx_buf;
#ifdef _USE_DMA_
ringbuf_dma_t usart1_rx_buf,usart2_rx_buf,usart6_rx_buf;
#else
ringbuff_t usart1_rx_buf,usart2_rx_buf,usart6_rx_buf;
#endif
/*
 * USART初期化
 * ch:1,2,3
 * brr:baudrate
 */
void init_usart(int ch,uint32_t brr){
	switch (ch){
	case 1:
		//GPIO設定
		//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO,ENABLE);//GPIO clock設定
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
		gpio_init_for_uart(GPIOA,GPIO_Pin_9,GPIO_Pin_10);//GPIO設定
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1);
		//USART設定
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART1,ENABLE);//usart有効
		usartmodule_init(USART1,brr);//usart設定
		//USART_ITConfig(USART1,USART_IT_TXE,ENABLE);//送信完了割り込み
#ifdef _USE_DMA_
		//DMA設定(受信バッファ)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2,ENABLE);//DMA clk
		dma_init_for_uart(DMA2_Stream2,USART1,(uintptr_t)usart1_rx_buf.data,DMA_Channel_4);//DMA init
		USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE);
		DMA_Cmd(DMA2_Stream2,ENABLE);//ENABLE DMA
#else
		USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
#endif
		//NVIC設定

		nvic_init_for_uart(USART1_IRQn);//ENABLE irqn
		//バッファ初期化
#ifdef _USE_DMA_
		usart1_rx_buf.read_ptr=0;
#else
		usart1_rx_buf.read_p=0;
		usart1_rx_buf.write_p=0;
#endif
		usart1_tx_buf.state=USART_NOT_SENDING;
		usart1_tx_buf.read_p=0;
		usart1_tx_buf.write_p=0;
		break;

	case 2:
		//GPIO設定
		//GPIO clock設定
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
		gpio_init_for_uart(GPIOA,GPIO_Pin_2,GPIO_Pin_3);//GPIO設定
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_USART2);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_USART2);
		//USART設定
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART2,ENABLE);//usart有効
		usartmodule_init(USART2,brr);//usart設定
		//USART_ITConfig(USART2,USART_IT_TXE,ENABLE);//送信完了割り込み
#ifdef _USE_DMA_
		//DMA設定(受信バッファ)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1,ENABLE);//DMA clk
		dma_init_for_uart(DMA1_Stream5,USART2,(uint32_t)usart2_rx_buf.data,DMA_Channel_4);//DMA init
		DMA_Cmd(DMA1_Stream5,ENABLE);
		USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);//ENABLE DMA
#else
		USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
#endif
		//NVIC設定
		nvic_init_for_uart(USART2_IRQn);//ENABLE irqn
		//バッファ初期化
#ifdef _USE_DMA_
		usart2_rx_buf.read_ptr=0;
#else
		usart2_rx_buf.read_p=0;
		usart2_rx_buf.write_p=0;
#endif
		usart2_tx_buf.state=USART_NOT_SENDING;
		usart2_tx_buf.read_p=0;
		usart2_tx_buf.write_p=0;
		break;
	case 6:
		//GPIO設定
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);//GPIO clock設定
		gpio_init_for_uart(GPIOC,GPIO_Pin_6,GPIO_Pin_7);//GPIO設定
		GPIO_PinAFConfig(GPIOC,GPIO_PinSource6,GPIO_AF_USART6);
		GPIO_PinAFConfig(GPIOC,GPIO_PinSource7,GPIO_AF_USART6);
		//USART設定
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART6,ENABLE);//usart有効
		usartmodule_init(USART6,brr);//usart設定
		//USART_ITConfig(USART6,USART_IT_TXE,ENABLE);//送信完了割り込み
#ifdef _USE_DMA_
		//DMA設定(受信バッファ)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2,ENABLE);//DMA clk
		dma_init_for_uart(DMA2_Stream1,USART6,(uint32_t)usart6_rx_buf.data,DMA_Channel_5);//DMA init
		DMA_Cmd(DMA2_Stream1,ENABLE);
		USART_DMACmd(USART6,USART_DMAReq_Rx,ENABLE);//ENABLE DMA
#else
		USART_ITConfig(USART6,USART_IT_RXNE,ENABLE);
#endif
		//NVIC設定
		nvic_init_for_uart(USART6_IRQn);//ENABLE irqn
		//バッファ初期化
#ifdef _USE_DMA_
		usart6_rx_buf.read_ptr=0;
#else
		usart6_rx_buf.read_p=0;
		usart6_rx_buf.write_p=0;
#endif
		usart6_tx_buf.state=USART_NOT_SENDING;
		usart6_tx_buf.read_p=0;
		usart6_tx_buf.write_p=0;
		break;
	default:
		break;
	}
}
/*
 * 1文字送信
 */
void usart1_putc(char c){
	__disable_irq();
	if(usart1_tx_buf.state==USART_NOT_SENDING){
	//atomic
	USART_SendData(USART1,(uint16_t)c);
	USART_ITConfig(USART1,USART_IT_TXE,ENABLE);//送信完了割り込み許可
	usart1_tx_buf.state=USART_SENDING;//送信中にする
	}else{
		usart1_tx_buf.data[usart1_tx_buf.write_p]=c;
		usart1_tx_buf.write_p++;
		usart1_tx_buf.write_p&=BUFF_SIZE-1;
	}
	__enable_irq();
}
/*
 * １文字受信
 */
char usart1_getc_noblock(void){
	char c=0;
#ifdef _USE_DMA_
	if(usart1_rx_buf.read_ptr!=BUFF_SIZE-(DMA2_Stream2->NDTR&0xffff)){//データあり
		__disable_irq();
		c=(char)(usart1_rx_buf.data[usart1_rx_buf.read_ptr]&0xff);
		usart1_rx_buf.read_ptr++;
		usart1_rx_buf.read_ptr&=BUFF_SIZE-1;
		__enable_irq();
	}
#else
	if(usart1_rx_buf.read_p!=usart1_rx_buf.write_p){
		__disable_irq();
		c=usart1_rx_buf.data[usart1_rx_buf.read_p];
		usart1_rx_buf.read_p++;
		usart1_rx_buf.read_p&=BUFF_SIZE-1;
		__enable_irq();
	}
#endif
	return c;//データがない場合は0を返す.
}
/*
 * 受信確認
 * 受信＝１
 */
int usart1_is_received(void){
#ifdef _USE_DMA_
	if(usart1_rx_buf.read_ptr!=BUFF_SIZE-(DMA2_Stream2->NDTR&0xffff)){
#else
		if(usart1_rx_buf.read_p!=usart1_rx_buf.write_p){
#endif
		return 1;
	}else{
		return 0;
	}
}
/*
 * １文字受信，受信してない場合は待つ
 */
char usart1_getc(void){
	char c;
#ifdef _USE_DMA_
	while(usart1_rx_buf.read_p==BUFF_SIZE-(DMA2_Stream2->NDTR&0xffff));
#else
	while(usart1_rx_buf.read_p==usart1_rx_buf.write_p);
#endif
	__disable_irq();
	c=(char)(usart1_rx_buf.data[usart1_rx_buf.read_p]&0xff);
	usart1_rx_buf.read_p++;
	usart1_rx_buf.read_p&=BUFF_SIZE-1;
	__enable_irq();
	return c;
}
/*
 * USART割り込み
 */
extern "C" void USART1_IRQHandler(void){//USART1割り込み処理
	char c;
	if(USART_GetITStatus(USART1,USART_IT_TXE)){
		if(usart1_tx_buf.read_p==usart1_tx_buf.write_p){
			USART_ITConfig(USART1,USART_IT_TXE,DISABLE);//バッファに送るデータがなくなった
			usart1_tx_buf.state=USART_NOT_SENDING;
		}else{
			c=usart1_tx_buf.data[usart1_tx_buf.read_p];
			USART_SendData(USART1,(uint16_t)c);
			usart1_tx_buf.read_p++;
			usart1_tx_buf.read_p&=BUFF_SIZE-1;
		}
	}
	if(USART_GetITStatus(USART1,USART_IT_RXNE)){
		c=(char)USART_ReceiveData(USART1);
		usart1_rx_buf.data[usart1_rx_buf.write_p]=c;
		usart1_rx_buf.write_p++;
		usart1_rx_buf.write_p&=BUFF_SIZE-1;
	}
}
void usart2_putc(char c){
	__disable_irq();
	if(usart2_tx_buf.state==USART_NOT_SENDING){
	//atomic
	USART_SendData(USART2,(uint16_t)c);
	USART_ITConfig(USART2,USART_IT_TXE,ENABLE);//送信完了割り込み許可
	usart2_tx_buf.state=USART_SENDING;//送信中にする
	}else{
		usart2_tx_buf.data[usart2_tx_buf.write_p]=c;
		usart2_tx_buf.write_p++;
		usart2_tx_buf.write_p&=BUFF_SIZE-1;
	}
	__enable_irq();
}
char usart2_getc_noblock(void){
	char c=0;
#ifdef _USE_DMA_
	if(usart2_rx_buf.read_ptr!=BUFF_SIZE-(DMA2_Stream5->NDTR&0xffff)){//データあり
		__disable_irq();
		c=(char)(usart2_rx_buf.data[usart2_rx_buf.read_ptr]&0xff);
		usart2_rx_buf.read_ptr++;
		usart2_rx_buf.read_ptr&=BUFF_SIZE-1;
		__enable_irq();
	}
#else
	if(usart2_rx_buf.read_p!=usart2_rx_buf.write_p){
		__disable_irq();
		c=usart2_rx_buf.data[usart2_rx_buf.read_p];
		usart2_rx_buf.read_p++;
		usart2_rx_buf.read_p&=BUFF_SIZE-1;
		__enable_irq();
	}
#endif
	return c;//データがない場合は0を返す.
}
int usart2_is_received(void){
	if(usart2_rx_buf.read_p!=usart2_rx_buf.write_p){
		return 1;
	}else{
		return 0;
	}
}
char usart2_getc(void){
	char c;
#ifdef _USE_DMA_
	while(usart2_rx_buf.read_p==BUFF_SIZE-(DMA2_Stream5->NDTR&0xffff));
#else
	while(usart2_rx_buf.read_p==usart2_rx_buf.write_p);
#endif
	__disable_irq();
	c=(char)(usart2_rx_buf.data[usart2_rx_buf.read_p]&0xff);
	usart2_rx_buf.read_p++;
	usart2_rx_buf.read_p&=BUFF_SIZE-1;
	__enable_irq();
	return c;
}
extern "C" void USART2_IRQHandler(void){//USART2割り込み処理
	char c;
	if(USART_GetITStatus(USART2,USART_IT_TXE)){
		if(usart2_tx_buf.read_p==usart2_tx_buf.write_p){
			USART_ITConfig(USART2,USART_IT_TXE,DISABLE);//バッファに送るデータがなくなった
			usart6_tx_buf.state=USART_NOT_SENDING;
		}else{
			c=usart2_tx_buf.data[usart2_tx_buf.read_p];
			USART_SendData(USART2,(uint16_t)c);
			usart2_tx_buf.read_p++;
			usart2_tx_buf.read_p&=BUFF_SIZE-1;
		}
	}
	if(USART_GetITStatus(USART2,USART_IT_RXNE)){
		c=(char)USART_ReceiveData(USART2);
		usart2_rx_buf.data[usart2_rx_buf.write_p]=c;
		usart2_rx_buf.write_p++;
		usart2_rx_buf.write_p&=BUFF_SIZE-1;
	}
}
void usart6_putc(char c){
	__disable_irq();
	if(usart6_tx_buf.state==USART_NOT_SENDING){//送信中でない
	USART_SendData(USART6,(uint16_t)c);
	USART_ITConfig(USART6,USART_IT_TXE,ENABLE);//送信完了割り込み許可
	usart6_tx_buf.state=USART_SENDING;//送信中にする
	}else{
		usart6_tx_buf.data[usart6_tx_buf.write_p]=c;
		usart6_tx_buf.write_p++;
		usart6_tx_buf.write_p&=BUFF_SIZE-1;
	}
	__enable_irq();
}
char usart6_getc_noblock(void){
	char c=0;
#ifdef _USE_DMA_
	if(usart6_rx_buf.read_ptr!=BUFF_SIZE-(DMA2_Stream6->NDTR&0xffff)){
#else
		if(usart6_rx_buf.read_p!=usart6_rx_buf.write_p){
#endif
		__disable_irq();
		c=(char)(usart6_rx_buf.data[usart6_rx_buf.read_p]&0xff);;
		usart6_rx_buf.read_p++;
		usart6_rx_buf.read_p&=BUFF_SIZE-1;
		__enable_irq();
	}
	return c;//データがない場合は0を返す.
}
int usart6_is_received(void){
#ifdef _USE_DMA_
	if(usart6_rx_buf.read_ptr!=BUFF_SIZE-(DMA2_Stream6->NDTR&0xffff)){
#else
		if(usart6_rx_buf.read_p!=usart6_rx_buf.write_p){
#endif
		return 1;
	}else{
		return 0;
	}
}
char usart6_getc(void){
	char c;
#ifdef _USE_DMA_
	while(usart6_rx_buf.read_p==BUFF_SIZE-(DMA2_Stream6->NDTR&0xffff));
#else
	while(usart6_rx_buf.read_p==usart6_rx_buf.write_p);
#endif
	__disable_irq();
	c=(char)(usart6_rx_buf.data[usart6_rx_buf.read_p]&0xff);
	usart6_rx_buf.read_p++;
	usart6_rx_buf.read_p&=BUFF_SIZE-1;
	__enable_irq();
	return c;
}
extern "C" void USART6_IRQHandler(void){//USART6割り込み処理
	char c;
	if(USART_GetITStatus(USART6,USART_IT_TXE)){
		if(usart6_tx_buf.read_p==usart6_tx_buf.write_p){
			USART_ITConfig(USART6,USART_IT_TXE,DISABLE);//バッファに送るデータがなくなった
			usart6_tx_buf.state=USART_NOT_SENDING;
		}else{
			c=usart6_tx_buf.data[usart6_tx_buf.read_p];
			USART_SendData(USART6,(uint16_t)c);
			usart6_tx_buf.read_p++;
			usart6_tx_buf.read_p&=BUFF_SIZE-1;
		}
	}
	if(USART_GetITStatus(USART6,USART_IT_RXNE)){
		c=(char)USART_ReceiveData(USART6);
		usart6_rx_buf.data[usart6_rx_buf.write_p]=c;
		usart6_rx_buf.write_p++;
		usart6_rx_buf.write_p&=BUFF_SIZE-1;
	}
}
