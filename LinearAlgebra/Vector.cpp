/*
 * Vector.cpp
 *
 *  Created on: 2015/01/10
 *      Author: 宗一郎
 */

#include "Vector.h"

#include <math.h>
#include <stdio.h>
Vector::Vector() {
	// TODO Auto-generated constructor stub
	dim=0;
	Value=0;//NullPtr
	refcount=new int;
	*refcount=0;
}
Vector::Vector(int dim){
	this->dim=dim;
	this->Value=new float[dim];
	this->refcount=new int;
	(*refcount)=1;
}
Vector::Vector(const Vector&origin){
	this->dim=origin.dim;
	this->Value=origin.Value;
	this->refcount=origin.refcount;
	(*(this->refcount))++;
}
float Vector::At(int i)const{
	return this->Value[i];
}
void Vector::At(int i,float val){
	this->Value[i]=val;
}
int Vector::Dims(void)const{
	return this->dim;
}
const Vector& Vector::operator=(const Vector&right){
	if(*(this->refcount)>1){
		(*(this->refcount))--;//今までのValueを参照しなくなる．refcount を減らす．
	}else if(*(this->refcount)==1){//thisインスタンスしかデータを参照していない．メモリをまず開放する．それから浅いコピ
		delete[]Value;
		delete refcount;
	}//(else refcount==0) デフォルトコンストラクタによる．refcountはdelする
	else{
		delete refcount;
	}
	this->dim=right.dim;
	this->Value=right.Value;
	this->refcount=right.refcount;
	(*(this->refcount))++;
	//printf("op:=,this->ref=%d,right->ref=%d\n\r",*(this->refcount),*(right.refcount));
	return (*this);
}
void Vector::CopyTo(Vector&destination){
	int i;
	if(*(destination.refcount)>1){//Value はdestinationに加えほかの場所から参照されている．開放してはいけない．refcountを減らす．
		(*(destination.refcount))--;
	}else if(*(destination.refcount)==1){//参照しているのがdestinationだけのばあい，メモリを解放してからコピーする．
		delete[]destination.Value;
		delete destination.refcount;
	}else{
		delete destination.refcount;
	}
	destination.Value=new float[dim];
	destination.dim=this->dim;
	for(i=0;i<dim;i++){
		destination.Value[i]=this->Value[i];
	}
	destination.refcount=new int;
	*(destination.refcount)=1;
}
const Vector Vector::operator+(const Vector&right)const{
	Vector result(this->dim);
	int i;
	for(i=0;i<dim;i++){
		result.Value[i]=this->Value[i]+right.Value[i];
	}
	return result;
}
const Vector Vector::operator-(const Vector&right)const{
	Vector result(this->dim);
	int i;
	for(i=0;i<dim;i++){
		result.Value[i]=this->Value[i]-right.Value[i];
	}
	return result;
}
float Vector::operator*(const Vector&right)const{
	int i;
	float result=0.0f;
	for(i=0;i<dim;i++){
		result+=this->Value[i]*right.Value[i];
	}
	return result;
}
const Vector Vector::operator*(const float scalar)const{
	Vector result(this->dim);
	int i;
	for(i=0;i<dim;i++){
		result.Value[i]=this->Value[i]*scalar;
	}
	return result;
}

const Vector operator*(const float scalar,const Vector&right){
	Vector result(right.dim);
	int i;
	for(i=0;i<right.dim;i++){
		result.Value[i]=right.Value[i]*scalar;
	}
	return result;
}
const Vector Vector::operator/(float scalar)const{
	Vector result(this->dim);
	int i=0;
	for(i=0;i<this->dim;i++){
		result.Value[i]=this->Value[i]/scalar;
	}
	return result;
}
const Vector Vector::operator^(const Vector&right)const{
	Vector result(3);
	result.Value[0]=this->Value[1]*right.Value[2]-this->Value[2]*right.Value[1];
	result.Value[1]=this->Value[2]*right.Value[0]-this->Value[0]*right.Value[2];
	result.Value[2]=this->Value[0]*right.Value[1]-this->Value[1]*right.Value[0];
	return result;
}
float Vector::Norm(void)const{
	float sum=0.0f;
	int i;
	for(i=0;i<this->dim;i++){
		float x=this->Value[i];
		sum+=x*x;
	}
	return sqrtf(sum);
}
const Vector Vector::Normalized(void){
	float norm=this->Norm();
	if(norm!=0.0f){
		return (*this)/norm;
	}else{
		return *this;
	}
}
void Vector::Normalize(void){
	float norm=this->Norm();
	int i;
	if(norm!=0.0f)
	for(i=0;i<dim;i++){
		this->Value[i]=this->Value[i]/norm;
	}
}
void Vector::Print(void)const{
	int i;
	for(i=0;i<this->dim;i++){
		printf("%f\n\r",this->Value[i]);
	}

}
Vector::~Vector() {
	// TODO Auto-generated destructor stub
	if(*refcount==0){//refcountが0 デフォルトコンストラクタで生成のちすぐ寿命が来た．Valueはnewされていないのでdeleteしてはいけない．
		delete refcount;
		//	printf("destructor,refcount=0\n\r");
		return;
	}
	//printf("destructor,decrement refcount\n\r");
	*refcount=*refcount-1;
	int i,j;
	if(*refcount==0){
		//	printf("Val:delete\n\r");
		delete[]this->Value;
		delete refcount;
	}
}

