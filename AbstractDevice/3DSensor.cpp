/*
 * Accelerometer.cpp
 *
 *  Created on: 2015/01/10
 *      Author: �@��Y
 */

#include "3DSensor.h"


Sensor3D::Sensor3D(){
	this->GainX=this->GainY=this->GainZ=0.0f;
	this->OffsetX=this->OffsetY=this->OffsetZ=0;
	x=y=z=0.0f;
}
Sensor3D::Sensor3D(float GainX,float GainY,float GainZ,int OffsetX,int OffsetY,int OffsetZ){
	this->GainX=GainX;
	this->GainY=GainY;
	this->GainZ=GainZ;
	this->OffsetX=OffsetX;
	this->OffsetY=OffsetY;
	this->OffsetZ=OffsetZ;
	x=y=z=0.0f;
}
void Sensor3D::SetGain(float GainX,float GainY,float GainZ){
	this->GainX=GainX;
	this->GainY=GainY;
	this->GainZ=GainZ;
}
void Sensor3D::SetOffset(int OffsetX,int OffsetY,int OffsetZ){
	this->OffsetX=OffsetX;
	this->OffsetY=OffsetY;
	this->OffsetZ=OffsetZ;
}
void Sensor3D::SetRawValue(int x,int y,int z){
	this->x=GainX*(x-OffsetX);
	this->y=GainY*(y-OffsetY);
	this->z=GainZ*(z-OffsetZ);
}
void Sensor3D::Get(float& x,float& y,float& z){
	x=this->x;
	y=this->y;
	z=this->z;
}
Vector Sensor3D::Get(void){
	Vector result(3);
	result.At(0,x);
	result.At(1,y);
	result.At(2,z);
	return result;
}
