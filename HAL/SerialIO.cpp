/*
 * SerialIO.cpp
 *
 *  Created on: 2015/08/28
 *      Author: 宗一郎
 */

#include "SerialIO.h"
#include "StreamIOManager.h"
#include "syscalls.h"
#include <fcntl.h>
SerialIO::SerialIO():fd(0),max_buff_len(0),input_buff(0),refcount(0),inputbuffIndex(0),recv_fin(false) {
	// TODO Auto-generated constructor stub
}
SerialIO::SerialIO(const SerialIO&origin){
	this->fd=origin.fd;
	this->input_buff=origin.input_buff;
	this->max_buff_len=origin.max_buff_len;
	this->refcount=origin.refcount;
	if(this->refcount){
		*(this->refcount)=*(this->refcount)+1;
	}
	this->recv_fin=origin.recv_fin;
	this->inputbuffIndex=origin.inputbuffIndex;
}
SerialIO::SerialIO(char*Name,int input_buf_size):fd(0),max_buff_len(input_buf_size),input_buff(0),refcount(0),inputbuffIndex(0),recv_fin(false){
	StreamIOinfo_t*info=StreamIO_retrival_by_Name(Name);
	if(info){
		fd=info->fd;
		_open(Name,O_NONBLOCK|O_BINARY);
		input_buff=new char[max_buff_len];
		refcount=new int;
		*refcount=1;
	}else{
		max_buff_len=0;
	}
}
void SerialIO::UpdateRequest(void){
	inputbuffIndex=0;
	recv_fin=false;
	_write(fd,"#",1);//文字'#'だけを送信
}

bool SerialIO::IsUpdateFinished(void){
	if(recv_fin){
		return true;
	}
	int readedsize=_read(fd,input_buff+inputbuffIndex,max_buff_len-1-inputbuffIndex);
	inputbuffIndex+=readedsize;
	char lastRecvChar='\0';
	if(inputbuffIndex>0){
		lastRecvChar=input_buff[inputbuffIndex-1];
	}
	if((inputbuffIndex==max_buff_len-1)||(lastRecvChar=='\n')){//次に書き込む場所がバッファの末尾または先ほど受信した文字が改行コード
		input_buff[inputbuffIndex]=0;//\0を付加して終了
		recv_fin=true;
		return true;
	}else{
		return false;
	}
}
char*SerialIO::GetReceivedString(void){
	return input_buff;
}
SerialIO::~SerialIO() {
	if(refcount){
		*refcount=*refcount-1;
		if(*refcount==0){
			delete refcount;
			delete input_buff;
		}
	}
	// TODO Auto-generated destructor stub
}

