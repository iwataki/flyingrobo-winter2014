/*
 * ComplementaryFlter.h
 *
 *  Created on: 2015/01/10
 *      Author: 宗一郎
 */

#ifndef COMPLEMENTARYFLTER_H_
#define COMPLEMENTARYFLTER_H_
template<typename T_State>
class ComplementaryFlter {
private:
	float alpha;
	T_State State;
public:
	ComplementaryFlter();
	ComplementaryFlter(float alpha);
	ComplementaryFlter(float alpha,T_State InitialState);
	void SetAlpha(float alpha);
	void Initialize(T_State);
	/**
	 * @brief 相補フィルタ
	 * 更新式
	 * State[t]=alpha(State[t-1]+StateDelta)+(1-alpha)StateCorrection
	 * @param State_Delta
	 * @param State_Correction
	 * @return
	 */
	T_State Execute(T_State StateDelta,T_State StateCorrection);
	void SaveState(T_State prev);
	virtual ~ComplementaryFlter();
};

template<typename T_State>
ComplementaryFlter<T_State>::ComplementaryFlter(){
	alpha=0.0f;
}
template<typename T_State>
ComplementaryFlter<T_State>::ComplementaryFlter(float alpha,T_State InitialState){
	this->alpha=alpha;
	this->State=InitialState;
}
template<typename T_State>
ComplementaryFlter<T_State>::ComplementaryFlter(float alpha){
	this->alpha=alpha;
}
template<typename T_State>
void ComplementaryFlter<T_State>::Initialize(T_State InitialState){
	this->State=InitialState;
}
template<typename T_State>
void ComplementaryFlter<T_State>::SetAlpha(float alpha){
	this->alpha=alpha;
}
template<typename T_State>
void ComplementaryFlter<T_State>::SaveState(T_State prev){
	this->State=prev;
}
template<typename T_State>
T_State ComplementaryFlter<T_State>::Execute(T_State StateDelta,T_State StateCorrection){
	T_State result;
	result=this->alpha*(this->State+StateDelta)+(1.0f-this->alpha)*StateCorrection;
	return result;
}
template<typename T_State>
ComplementaryFlter<T_State>::~ComplementaryFlter(){

}
#endif /* COMPLEMENTARYFLTER_H_ */
