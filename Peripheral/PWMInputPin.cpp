/*
 * PWMInputPin.cpp
 *
 *  Created on: 2014/12/30
 *      Author: 岩滝　宗一郎
 */

#include "PWMInputPin.h"

PWMInputPin*TIM1_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM2_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM3_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM4_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM5_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM6_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM7_PWMpins[4]={0,0,0,0};
PWMInputPin*TIM8_PWMpins[4]={0,0,0,0};

PWMInputPin::PWMInputPin(GPIO_TypeDef*Port,int GPIO_Pin_x,TIM_TypeDef*Timer,int IC_ch,int Period){
	SignalOk=false;
	Nopulse=0;
	GPIO_InitTypeDef initgpio;
	initgpio.GPIO_Mode=GPIO_Mode_AF;
	initgpio.GPIO_OType=GPIO_OType_PP;
	initgpio.GPIO_Pin=GPIO_Pin_x;
	initgpio.GPIO_Speed=GPIO_High_Speed;
	duty=0;
	this->ch=IC_ch;
	this->timer=Timer;
	this->period=Period;
	int pin=0;
	for(pin=0;pin<0x10;pin++){
		if((1<<pin)&GPIO_Pin_x){
			break;
		}
	}
	if(Port==GPIOA){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	}else if(Port==GPIOB){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	}else if(Port==GPIOC){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	}else if(Port==GPIOD){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	}else if(Port==GPIOE){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);
	}else if(Port==GPIOF){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	}else if(Port==GPIOG){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG,ENABLE);
	}else if(Port==GPIOH){
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	}
	int AFflag;
	RCC_ClocksTypeDef clkstatus;
	RCC_GetClocksFreq(&clkstatus);
	int clkfreq;
	NVIC_InitTypeDef initnvic;
	if(Timer==TIM1){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
		AFflag=GPIO_AF_TIM1;
		clkfreq=clkstatus.PCLK2_Frequency;
		TIM1_PWMpins[ch-1]=this;
		initnvic.NVIC_IRQChannel=TIM1_CC_IRQn;
	}else if(Timer==TIM2){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
		AFflag=GPIO_AF_TIM2;
		clkfreq=clkstatus.PCLK1_Frequency;
		TIM2_PWMpins[ch-1]=this;
		initnvic.NVIC_IRQChannel=TIM2_IRQn;
	}else if(Timer==TIM3){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
		AFflag=GPIO_AF_TIM3;
		clkfreq=clkstatus.PCLK1_Frequency;
		TIM3_PWMpins[ch-1]=this;
		initnvic.NVIC_IRQChannel=TIM3_IRQn;
	}else if(Timer==TIM4){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
		AFflag=GPIO_AF_TIM4;
		clkfreq=clkstatus.PCLK1_Frequency;
		TIM4_PWMpins[ch-1]=this;
		initnvic.NVIC_IRQChannel=TIM4_IRQn;
	}else if(Timer==TIM5){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5,ENABLE);
		AFflag=GPIO_AF_TIM5;
		clkfreq=clkstatus.PCLK1_Frequency;
		TIM5_PWMpins[ch-1]=this;
		initnvic.NVIC_IRQChannel=TIM5_IRQn;
	}else if(Timer==TIM6){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);
	}else if(Timer==TIM7){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7,ENABLE);
	}else if(Timer==TIM8){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8,ENABLE);
		AFflag=GPIO_AF_TIM8;
		clkfreq=clkstatus.PCLK2_Frequency;
		TIM8_PWMpins[ch-1]=this;
	}
	initnvic.NVIC_IRQChannelCmd=ENABLE;
	initnvic.NVIC_IRQChannelPreemptionPriority=3;
	NVIC_Init(&initnvic);
	clkfreq=2*clkfreq;
	GPIO_Init(Port,&initgpio);
	GPIO_PinAFConfig(Port,pin,AFflag);
	TIM_TimeBaseInitTypeDef timebaseinit;
	TIM_ICInitTypeDef ICinit;
	TIM_BDTRInitTypeDef bdtrinit;
	timebaseinit.TIM_ClockDivision=TIM_CKD_DIV1;
	timebaseinit.TIM_CounterMode=TIM_CounterMode_Up;
	timebaseinit.TIM_Period=0xffff;
	timebaseinit.TIM_Prescaler=((clkfreq/1000000)*Period)/0xffff;
	timebaseinit.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(Timer,&timebaseinit);
	//ICinit.TIM_Channel=(OC_ch-1)*4;
	ICinit.TIM_ICFilter=0x08;
	ICinit.TIM_ICPolarity=TIM_ICPolarity_Rising;
	ICinit.TIM_ICPrescaler=TIM_ICPSC_DIV1;
	ICinit.TIM_ICSelection=TIM_ICSelection_DirectTI;
	TIM_Cmd(Timer,ENABLE);
	switch(IC_ch){
	case 1:
		//TIM_OC1Init(Timer,&OCinit);
		ICinit.TIM_Channel=TIM_Channel_1;
		TIM_ICInit(Timer,&ICinit);
		ICinit.TIM_Channel=TIM_Channel_2;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Falling;
		ICinit.TIM_ICSelection=TIM_ICSelection_IndirectTI;
		TIM_ICInit(Timer,&ICinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);
		TIM_CCxCmd(Timer,TIM_Channel_2,TIM_CCx_Enable);
		TIM_ITConfig(Timer,TIM_IT_CC2,ENABLE);
		break;
	case 2:
		//TIM_OC2Init(Timer,&OCinit);
		ICinit.TIM_Channel=TIM_Channel_1;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Falling;
		ICinit.TIM_ICSelection=TIM_ICSelection_IndirectTI;
		TIM_ICInit(Timer,&ICinit);
		ICinit.TIM_Channel=TIM_Channel_2;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Rising;
		ICinit.TIM_ICSelection=TIM_ICSelection_DirectTI;
		TIM_ICInit(Timer,&ICinit);
		TIM_CCxCmd(Timer,TIM_Channel_1,TIM_CCx_Enable);
		TIM_CCxCmd(Timer,TIM_Channel_2,TIM_CCx_Enable);
		TIM_ITConfig(Timer,TIM_IT_CC1,ENABLE);
		break;
	case 3:
		ICinit.TIM_Channel=TIM_Channel_3;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Rising;
		ICinit.TIM_ICSelection=TIM_ICSelection_DirectTI;
		TIM_ICInit(Timer,&ICinit);
		ICinit.TIM_Channel=TIM_Channel_4;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Falling;
		ICinit.TIM_ICSelection=TIM_ICSelection_IndirectTI;
		TIM_ICInit(Timer,&ICinit);
		TIM_CCxCmd(Timer,TIM_Channel_3,TIM_CCx_Enable);
		TIM_CCxCmd(Timer,TIM_Channel_4,TIM_CCx_Enable);
		TIM_ITConfig(Timer,TIM_IT_CC4,ENABLE);
		break;
	case 4:
		ICinit.TIM_Channel=TIM_Channel_3;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Falling;
		ICinit.TIM_ICSelection=TIM_ICSelection_IndirectTI;
		TIM_ICInit(Timer,&ICinit);
		ICinit.TIM_Channel=TIM_Channel_4;
		ICinit.TIM_ICPolarity=TIM_ICPolarity_Rising;
		ICinit.TIM_ICSelection=TIM_ICSelection_DirectTI;
		TIM_ICInit(Timer,&ICinit);
		TIM_CCxCmd(Timer,TIM_Channel_3,TIM_CCx_Enable);
		TIM_CCxCmd(Timer,TIM_Channel_4,TIM_CCx_Enable);
		TIM_ITConfig(Timer,TIM_IT_CC3,ENABLE);
		break;
	}
	TIM_ITConfig(Timer,TIM_IT_Update,ENABLE);

}
unsigned int PWMInputPin::Get(void){
	unsigned int retval=duty*period/0xffff;
	return retval;
}

void PWMInputPin::IRQHandler(void){
	if(TIM_GetITStatus(timer,TIM_IT_CC1+TIM_IT_CC2+TIM_IT_CC3+TIM_IT_CC4)){
		SignalOk=true;
		Nopulse=0;
		switch(ch){
		case 1:
			duty=(TIM_GetCapture2(this->timer)-TIM_GetCapture1(this->timer))&0x0000ffff;
			//TIM_SetCompare1(timer,0);
			//TIM_SetCompare2(timer,0);
			break;
		case 2:
			duty=(TIM_GetCapture1(this->timer)-TIM_GetCapture2(this->timer))&0x0000ffff;
			//TIM_SetCompare1(timer,0);
			//TIM_SetCompare2(timer,0);
			break;
		case 3:
			duty=(TIM_GetCapture4(this->timer)-TIM_GetCapture3(this->timer))&0x0000ffff;
			//TIM_SetCompare3(timer,0);
			//TIM_SetCompare4(timer,0);
			break;
		case 4:
			duty=(TIM_GetCapture3(this->timer)-TIM_GetCapture4(this->timer))&0x0000ffff;
			//TIM_SetCompare3(timer,0);
			//TIM_SetCompare4(timer,0);
			break;
		}
	}

		//TIM_ClearITPendingBit(timer,TIM_IT_Update);ここではクリアすべきではない

	if(Nopulse>2){
		SignalOk=false;
	}else{
		Nopulse++;
	}

}
bool PWMInputPin::IsValid(void){
	return SignalOk;
}


void TIM1_CC_IRQHandler(void){
	if(TIM_GetITStatus(TIM1,TIM_IT_CC1)){
		TIM1_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM1,TIM_IT_CC2)){
		TIM1_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM1,TIM_IT_CC3)){
		TIM1_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM1,TIM_IT_CC4)){
		TIM1_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM1,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM1_PWMpins[i]!=0){
				TIM1_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
}


void TIM2_IRQHandler(void){
	if(TIM_GetITStatus(TIM2,TIM_IT_CC1)){
		TIM2_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_CC2)){
		TIM2_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_CC3)){
		TIM2_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM2,TIM_IT_CC4)){
		TIM2_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM2,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM2_PWMpins[i]!=0){
				TIM2_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
}


void TIM3_IRQHandler(void){
	if(TIM_GetITStatus(TIM3,TIM_IT_CC1)){
		TIM3_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM3,TIM_IT_CC2)){
		TIM3_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM3,TIM_IT_CC3)){
		TIM3_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM3,TIM_IT_CC4)){
		TIM3_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM3_PWMpins[i]!=0){
				TIM3_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
}


void TIM4_IRQHandler(void){
	if(TIM_GetITStatus(TIM4,TIM_IT_CC1)){
		TIM4_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM4,TIM_IT_CC2)){
		TIM4_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM4,TIM_IT_CC3)){
		TIM4_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM4,TIM_IT_CC4)){
		TIM4_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM4,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM4_PWMpins[i]!=0){
				TIM4_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
}


void TIM5_IRQHandler(void){
	if(TIM_GetITStatus(TIM5,TIM_IT_CC1)){
		TIM5_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM5,TIM_IT_CC2)){
		TIM5_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM5,TIM_IT_CC3)){
		TIM5_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM5,TIM_IT_CC4)){
		TIM5_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM5,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM5_PWMpins[i]!=0){
				TIM5_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM5,TIM_IT_Update);
}


void TIM6_IRQHandler(void){
	if(TIM_GetITStatus(TIM6,TIM_IT_CC1)){
		TIM6_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM6,TIM_IT_CC2)){
		TIM6_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM6,TIM_IT_CC3)){
		TIM6_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM6,TIM_IT_CC4)){
		TIM6_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM6,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM6_PWMpins[i]!=0){
				TIM6_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
}


void TIM7_IRQHandler(void){
	if(TIM_GetITStatus(TIM7,TIM_IT_CC1)){
		TIM7_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM7,TIM_IT_CC2)){
		TIM7_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM7,TIM_IT_CC3)){
		TIM7_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM7,TIM_IT_CC4)){
		TIM7_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM7,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM7_PWMpins[i]!=0){
				TIM7_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM7,TIM_IT_Update);
}


void TIM8_CC_IRQHandler(void){
	if(TIM_GetITStatus(TIM8,TIM_IT_CC1)){
		TIM8_PWMpins[1]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM8,TIM_IT_CC2)){
		TIM8_PWMpins[0]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM8,TIM_IT_CC3)){
		TIM8_PWMpins[3]->IRQHandler();
	}
	if(TIM_GetITStatus(TIM8,TIM_IT_CC4)){
		TIM8_PWMpins[2]->IRQHandler();
	}
	int i=0;
	if(TIM_GetITStatus(TIM8,TIM_IT_Update)){
		for(i=0;i<4;i++){
			if(TIM8_PWMpins[i]!=0){
				TIM8_PWMpins[i]->IRQHandler();
			}
		}
	}
	TIM_ClearITPendingBit(TIM8,TIM_IT_Update);
}


