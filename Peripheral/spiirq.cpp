/*
 * spiirq.cpp
 *
 *  Created on: 2014/12/18
 *      Author: �@��Y
 */




#include "spi.h"
#include <stdio.h>
SPI*spi1;
void SPI1_IRQSet(SPI*target){
	spi1=target;
}
extern"C"{
	void SPI1_IRQHandler(void){
		//printf("SPI_interupt\n\r");
		if(SPI_I2S_GetITStatus(SPI1,SPI_I2S_IT_RXNE)){
			spi1->IRQ();
		}
	}
}
SPI*spi2;
void SPI2_IRQSet(SPI*target){
	spi2=target;
}
extern"C"{
	void SPI2_IRQHandler(void){
		spi2->IRQ();
	}
}
